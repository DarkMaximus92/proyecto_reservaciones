import { Component, ViewChild, OnInit, ElementRef, Renderer2 } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { 
  NgbInputDatepicker, 
  NgbDateStruct, 
  NgbCalendar, 
  NgbDate,
  NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { PropiedadesService } from '../propiedades.service';
import {NgModel} from "@angular/forms";
import * as moment from 'moment';
import { AlertController } from '@ionic/angular';

const now = new Date();
const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;

@Component({
  selector: 'app-reservar',
  templateUrl: './reservar.page.html',
  styleUrls: ['./reservar.page.scss'],
})

export class ReservarPage implements OnInit {

  title = 'angular-bootstrap-datepicker-tutorial';
  fromDate: any;
  toDate: any;
  today: NgbDate | null;
  hoveredDate: NgbDateStruct;

  public id: string;

  fechas: any;
  fechasReservadas:NgbDateStruct[] = [];

  model: any;
  @ViewChild("d") input: NgbInputDatepicker;
  @ViewChild(NgModel) datePick: NgModel;
  @ViewChild('myRangeInput') myRangeInput: ElementRef;

  isDisabled = (date: NgbDateStruct, current: {month: number,year: number})=> {
    return this.fechasReservadas.find(x => NgbDate.from(x).equals(date))? true: false;
  }

  isHovered = date => 
    this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate)
    isInside = date => after(date, this.fromDate) && before(date, this.toDate);
    isFrom = date => equals(date, this.fromDate);
    isTo = date => equals(date, this.toDate);

  constructor(
    private calendar: NgbCalendar, 
    public formatter: NgbDateParserFormatter, 
    private route: ActivatedRoute, 
    private router: Router, 
    private servicio: PropiedadesService,
    element: ElementRef, 
    private renderer: Renderer2, 
    private _parserFormatter: NgbDateParserFormatter,
    public alertController: AlertController)
    {
      this.today = calendar.getToday();
      this.fromDate = calendar.getToday();
      this.toDate = calendar.getNext(calendar.getToday(), 'd', 1);
      //this.fromDate = null;
      //this.toDate = null;
  }

  async presentAlert1() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Exito',
      subHeader: '-',
      message: 'Su reservacion ha sido realizada con exito',
      buttons: [{
        text: 'OK',
        handler: () => {
          window.location.href = '/propiedades';
        }
      }]
    });
    await alert.present();
  }

  async presentAlert2() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Error',
      subHeader: '-',
      message: 'Ha seleccionado un rango de fechas que contiene una fecha reservada, favor elija un rango de fechas que tenga todas sus fechas disponibles',
      buttons: ['OK']
    });
    await alert.present();
  }

  onDateSelection(date: NgbDateStruct) {
    let parsed = '';
    if (!this.fromDate && !this.toDate) {
        this.fromDate = date;
    } else if (this.fromDate && !this.toDate && after(date, this.fromDate)) {
        this.toDate = date;
        this.model = `${this.fromDate.year} - ${this.toDate.year}`;
        this.input.close();
    } else {
        this.toDate = null;
        this.fromDate = date;
    }
    if(this.fromDate) {
      parsed += this._parserFormatter.format(this.fromDate);
    }
    if(this.toDate) {
      parsed += ' - ' + this._parserFormatter.format(this.toDate);
    }
  
    this.renderer.setProperty(this.myRangeInput.nativeElement, 'value', parsed);
    
  }

  onClick(){

    var arr = [];

    var currentDate = moment(this.fromDate);
    var stopDate = moment(this.toDate);
    while (currentDate <= stopDate) {
        const ob = {
          year: currentDate.year(),
          month: currentDate.month(),
          day: currentDate.date(),
        };
        arr.push(ob)
        currentDate = moment(currentDate).add(1, 'days');
    }

    console.log(arr);
    console.log(this.fechasReservadas);

    var parar = false;

    for(let i = 0; i<this.fechasReservadas.length; i++){
      for(let j = 0; j<arr.length; j++){
        if(this.fechasReservadas[i].year == arr[j].year 
          && this.fechasReservadas[i].month == arr[j].month 
          && this.fechasReservadas[i].day == arr[j].day){
          parar = true;
          stop;
        }
      }
    }

    if(!parar){
      this.servicio.reservar(this.id, JSON.stringify(this.fromDate), JSON.stringify(this.toDate), arr);
      this.presentAlert1();
    } else {
      this.presentAlert2();
    }
    
  }

  ngOnInit() {  
    this.route.paramMap.subscribe(paramMap => {
      if(!paramMap.has('idPropiedad')){
        return;
      }
      this.id = paramMap.get('idPropiedad');
    });

    this.fechasReservadas = this.servicio.getFechasReservadas(this.id);
    console.log(this.fechasReservadas);    
  }

  

}
