import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PropiedadesPage } from './propiedades.page';

import { EditarResolver } from './editar/editar.resolver';
import { DetalleResolver } from './detalle/detalle.resolver';

const routes: Routes = [
  {
    path: '',
    component: PropiedadesPage
  },
  {
    path: 'detalle',
    children:[
      {
        path: '',
        loadChildren: () => import('./detalle/detalle.module').then( m => m.DetallePageModule)
      },
      {
        path: ':idPropiedad',
        loadChildren: () => import('./detalle/detalle.module').then( m => m.DetallePageModule),
        resolve:{data : DetalleResolver}             
      }
    ]
  },
  {
    path: 'agregar',
    loadChildren: () => import('./agregar/agregar.module').then( m => m.AgregarPageModule)
  },
  {
    path: 'editar',
    children:[
      {
        path: '',
        loadChildren: () => import('./editar/editar.module').then( m => m.EditarPageModule)
      },
      {
        path: ':idPropiedad',
        loadChildren: () => import('./editar/editar.module').then( m => m.EditarPageModule),
        resolve:{data : EditarResolver}               
      }
    ]
  },
  {
    path: 'reservar',
    children:[
      {
        path: '',
        loadChildren: () => import('./reservar/reservar.module').then( m => m.ReservarPageModule)
      },
      {
        path: ':idPropiedad',
        loadChildren: () => import('./reservar/reservar.module').then( m => m.ReservarPageModule),
      }
    ]
  },
  {            
    path: 'byUser',
    children:[
      {
        path: '',
        loadChildren: () => import('./byUser/byUser.module').then( m => m.ByUserPageModule)
      },
      {
        path: ':uid',
        loadChildren: () => import('./byUser/byUser.module').then( m => m.ByUserPageModule),
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PropiedadesPageRoutingModule {}
